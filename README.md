Codes relating to the article "Approximate Bayesian Computation applied to time series of population genetic data disentangles rapid genetic changes and demographic variations in a pathogen population".

These codes are designed to validate and perform model choice and parameter inferences by ABC analysis, from simulated data and biological data of the RMlp7 resistance overcoming by the fungal pathogen *Melampsora larici-populina*.

A first description of each script is provided by alphabetical order. Then, a description of each provided file is given by alphabetical order. Finally, a more general description of the project is provided with the order in which the scripts must be executed to obtain the figures and data presented in the article.

# Script Descriptions

1. `ABC_Analyses_Real_Datasets.R`
	
	ABC analyses (Model choice, parameter estimations, and ranking of indices influence) from real data.
	Analyses are performed on compartments S or S and R for the model choice, compartments S, S and A, S and R, or S, R and A for the parameter estimations.
	With two possible kinds of summary statistics: Complete (all generations, without post-treatment), Wrap-up (min, max, mean and variance).

	**Inputs:** 
	- Lists of indices chosen for the ABC analyses, for each host compartment
	- List of location to choose the adapted dataset (Amance or GrandEst locations)
	- List of choices for the summary statistics (wrapup or complete)
	- Tolerances for the model choice and parameter estimations
	- The number of replicates used to estimate the distribution of the goodness-of-fit statistic
	- Methods used for the model choice and parameter estimation
	- List of parameters to infer
	- Vectors of boundaries and transformations for parameter estimations
	- Tables of adapted simulated results (.Rdata)
	- Tables of adapted real datasets (.Rdata)

	**Outputs:**
	- Results of the model choice (.csv)
	- Graphics of goodness of fit for the model choice (.pdf)
	- Graphics of PCA areas for the model choice (.pdf)
	- Results of the parameter estimations (.Rdata)

2. `ABC_Cross_Validation_Real_Data_Structure.R`
	
	ABC cross validations (Model choice cross validation and parameter estimation cross validation) from simulated data with similar structure (years and compartments) than the real datasets
	Analyses are performed for two distinct structures of the data sets
	- structure similar to the real data set in Grand Est, with Complete summary statistics and compartments S R and A
	- structure similar to the real data set in Amance, with Complete summary statistics and compartments S and A

	**Inputs:** 
	- Lists of indices chosen for the ABC analyses, for each host compartment
	- List of location to choose the adapted dataset (Amance or GrandEst locations)
	- List of choices for the summary statistics (wrapup or complete)
	- Tolerances for the model choice and parameter estimations
	- Number of values to build the confusion matrix
	- Size of the cross-validation sample for the parameters estimations
	- Methods used for the model choice and parameter estimation
	- List of parameters to infer
	- Tables of adapted simulated results (.Rdata)
	- Tables of adapted real datasets (.Rdata)

	**Outputs:**
	- Confusion matrices for the model choice cross validation (.csv)
	- Results of the parameter estimations cross validation with estimated and true parameters (.csv)

3. `ABC_Cross_Validation_Simulations_Complete.R`
	
	ABC cross validations (Model choice cross validation and parameter estimation cross validation) from simulated data 
	Analyses are performed on compartments S or S and R for the model choice, compartments S, S and A, S and R, or S, R and A for the parameter estimations.
	For three temporal sampling: every year, every five years, or the first and last years only
	With the summary statistics: Complete (all generations, without post-treatment)

	**Inputs:** 
	- Lists of indices chosen for the ABC analyses, for each host compartment
	- Tolerances for the model choice and parameter estimations
	- Number of values to build the confusion matrix
	- Size of the cross-validation sample for the parameters estimations
	- Methods used for the model choice and parameter estimation
	- List of parameters to infer
	- List of the temporal samplings to consider (AllY: Every year, 5Y: Every five years, Firstlast: only the first and last years)
	- Tables of adapted simulated results (.Rdata)

	**Outputs:**
	- Confusion matrices for the model choice cross validation (.csv)
	- Graphics of PCA areas for the model choice (.pdf)
	- Results of the parameter estimations cross validation with estimated and true parameters (.csv)

4. `ABC_Cross_Validation_Simulations_Wrapup.R`
	
	ABC cross validations (Model choice cross validation and parameter estimation cross validation) from simulated data 
	Analyses are performed on compartments S or S and R for the model choice, compartments S, S and A, S and R, or S, R and A for the parameter estimations.
	For two temporal sampling: every year or every five years
	With the summary statistics: Wrap-up (min, max, mean and variance of the temporal data)

	**Inputs:** 
	- Lists of indices chosen for the ABC analyses, for each host compartment
	- Tolerances for the model choice and parameter estimations
	- Number of values to build the confusion matrix
	- Size of the cross-validation sample for the parameters estimations
	- Methods used for the model choice and parameter estimation
	- List of parameters to infer
	- List of the temporal samplings to consider (AllY: Every year, 5Y: Every five years)
	- Tables of adapted simulated results (.Rdata)

	**Outputs:**
	- Confusion matrices for the model choice cross validation (.csv)
	- Graphics of PCA areas for the model choice (.pdf)
	- Results of the parameter estimations cross validation with estimated and true parameters (.csv)

5. `abc_function.R`
	
	Resumption of certain functions from the [abc R package](https://cran.r-project.org/web/packages/abc/index.html) (Csillery et al. 2012) to modify parameters which were not arguments	
	Modification of functions abc (now abc_custom), cv4abc, (now cv4abc_custom) and gfitpca (now gfitpca_theorique). 
	- We increased the parameter MaxNWts to 84581 in the nnet function.
	- We change maxk to 200 (initially 100) in the cv4abc function

6. `Calculation_PopGen_Indices.R`
	
	Script to calculate some population genetic indices for the real datasets from R packages instead of the model simulator, to avoid missing data errors, for the two distinct locations Amance and GrandEst

	**Inputs:** 
	- Initial real dataset with microsatellite markers (TimeSerie_88-21.csv)

	**Outputs:**
	- Tables of population genetics indices from the real datasets, for the two locations (.Rdata)

7. `Demogenetic_model.exe`

	Executable file that takes as input a simulation design (input.txt), simulate the pathogen population evolution through time and give as output the temporal evolution of a set of population genetic indices on three host compartment: susceptuble (S), resistant (R) and alternate (host).

	**Inputs:** 
	Table of simulation design (named input.txt), with the following variables:
	- alternance: life cycle of the pathogenm with (True) or without (False) host alternation (Cycle)
	- tu: mutation rate of the neutral markers
	- tmig: migration rate of the pathogen between R and S host compartments (mig)
	- nbgenbi: number of generations to simulate for the burn-in period
	- nbgen: number of generations to simulate after the burn-in period
	- nblociN: number of neutral loci to simulate
	- talvir: initial proportion of virulent alleles in the pathogen population (favr)
	- nbcapchaSetR: cumulated carrying capacyty on S and R host compartments (K)
	- nbcapchaS: carrying capacity on the S compartment (KS)
	- nbcapchaR: carrying capacity on the R compartment (KR)
	- tr: pathogen population growth rate (r)
	- tmortMel: mortality rate during the annual migration (tau)

	**Outputs:**
	- Simulation results in a .txt file 


8. `Results_Graphs_and_Display.R`
	
	Script to display results as shown in the article

	**Inputs:** 
	- Result table of parameter estimation cross validation to analyse (.csv)
	- Initial experimental design (.txt)
	- Result dataset of parameter estimation to analyse (.Rdata)

	**Outputs:**
	- Graphs of estimated vs true parameter values for the cross validation of the parameter estimation, with associated correlation coefficients (.pdf)
	- Graphs of prior and posterior distributions of each parameters (.pdf)
	- Information about the posterior distribution of each inferred parameter: mode, median, mean and quantiles 2.5% and 97.5%

9. `Temporary_Tables_Creation.R`
	
	Script to create and transform Rdata tables used for all the ABC analyses

	**Inputs:** 
	- Initial simulation results (Simulation_dataset.Rdata)
	- Brut real datasets for two locations (Amance and GrandEst) (Donnees_reelles_Amance.txt, Donnees_reelles_GrandEst.txt)
	- Real data with calculations from poppr (Table_PPR_restreint_GdP_Amance.Rdata, Table_PPR_restreint_GdP_GrandEst.Rdata)

	**Outputs:**
	- Refined simulation table, used for further ABC analyses (Simulation_dataset.Rdata)
	- Refined real datasets for each location, used for further ABC analyses (.Rdata)
	- Summary statistic tables (Complete and Wrap-up) from simulation data (.Rdata)
	- Summary statistic tables (Complete and Wrap-up) from simulation data with the same structure than the real datasets (.Rdata)

10. `Transform_Brut_Results.R`
	
	Script to transform the initial Brut simulation table (from the .exe file) into the Rdata table used for the ABC analyses

	**Inputs:** 
	- Brut simulation table (.txt)

	**Outputs:**
	- Refined simulation table, used for further ABC analyses (Simulation_dataset.Rdata)

# Provided Files Description

1. Donnees_reelles_Amance.txt and Donnees_reelles_GrandEst.txt
	
	Real dataset with calculated population genetics indices for each pathogen population, for two distinct sampling locations: Amance and the Grand Est region.

2. RandomDesign.txt
	
	Random simulation design used in the article, consisting of 150 000 parameter combinations randomly drawn from defined distributions. 

3. Simulation_dataset.Rdata
	
	Simulated dataset from the random simulation design, restricted to simulations for which the virulent avr allele was still present at the end of the simulated generations. This table regroups for each simulation and each sampled generation the parameter values and the calculated population genetics indices on the three host compartments (S, R and A).

4. TimeSerie_88-21.csv
	
	Real dataset with microsatellite markets: temporal evolution of *Melampsora larici-populina* populations through time, before, during and after the resistance overcoming of poplar resistance RMlp7.

# General Code Description

Before any analysis, you can run the demogenetic simulator `Demogenetic_model.exe` with your own simulation design (input.txt), or with the simulation design we used in the article (RandomDesign.txt). Please note that as the demogenetic model is stochastic, the same simulation design will lead to different results. If you run the executor to obtain your own simulations results, start by transforming your brut simulation results file (.txt) into the refined simulation table used for further ABC analyses (Simulation_dataset.Rdata), with the script `Transform_Brut_Results.R`. Please note that this step is not needed if you aim to reproduce the results of the article, as the result file we used (Simulation_dataset.Rdata) is already provided. 

To obtain the results presented in the article, all scripts must be run in the following order without parameter modifications. Please note that some scripts can take a long time to be executed (from several hours to several days on a computer with 32Go RAM, Intel(R) Xeon(R) W-1390P @ 3.50GHz), they are indicated as **time-consuming** in the following description and the indicated time corresponds to the time it took to run the script under the specified computer configuration. For each script to run, choose as working directory your source file location.

Run the scripts responsible for table creation and transformations, in the following order:

	- `Calculation_PopGen_Indices.R`
	- `Temporary_Tables_Creation.R` (**time-consuming**: 11h)

Then, ABC analyses can be performed. These analyses regroup four distinct scripts that can be run independently. We recommend parallelising to save simulation time. 
Two scripts (Scripts 3. and 4.) perform the cross validations of the ABC method from non restricted simulated data, for the two distinct summary statistics (Complete and Wrap-up, respectively) presented in the article. Another script (Script 2.) performs the cross validation from simulated data presenting the same structure (years and compartments) thant the real datasets, for the two locations (GrandEst and Amance) considered in the article. A last script (Script 1.) performs the ABC inferences based on the real datasets, for the two locations considered in the article:

	- `ABC_Cross_Validation_Simulations_Complete.R` (**time-consuming**: 116h)
	- `ABC_Cross_Validation_Simulations_Wrapup.R` (**time-consuming**: 14h)
	- `ABC_Cross_Validation_Real_Data_Structure.R` (**time-consuming**: 39h)
	- `ABC_Analyses_Real_Datasets.R`

Finally, last graphics and information are obtained by running the script to display the results as shown in the article. For this script, each input file correspond to an output file from the ABC scripts. Adapts these inputs to the result you want to print:

	- `Results_Graphs_and_Display.R`

