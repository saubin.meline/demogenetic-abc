# ABC cross validations (Model choice cross validation and parameter estimation cross validation) 
# from simulated data with similar structure (years and compartments) than the real data 
# Analyses are performed for two distinct structures of the data sets
# - structure similar to the real data set in Grand Est, with Complete summary statistics and compartments S R and A
# - structure similar to the real data set in Amance, with Complete summary statistics and compartments S and A

library(abc) 
library(ggplot2)
library(stringr) # for str_replace
library(LaplacesDemon) # for the logit function

directory = getwd()

#################################################
#################### Inputs #####################
#################################################

# Indices lists for each compartment
Liste_indices_S = c("freq_virulent_S","GsurN_S", "lambdaetoile_S","SWinx_S","BetaPareto_S","MrbarDAR_S","MHe_S", "VarHe_S","MLA_S","Fst_temporel_S_BI")
Liste_indices_R = c("GsurN_R", "lambdaetoile_R","SWinx_R","BetaPareto_R","MrbarDAR_R","MHe_R", "VarHe_R","MLA_R","Fst_temporel_R_BI","Fst_R_S")
Liste_indices_A = c("freq_virulent_A","GsurN_A", "lambdaetoile_A","SWinx_A","BetaPareto_A","MrbarDAR_A","MHe_A", "VarHe_A","MLA_A","Fst_temporel_A_BI")

# Location list
liste_lieux = c("GrandEst", "Amance")

# summary statistics list
liste_stats = c("wrapup", "complete")
# Nb of values to build the confusion matrix
NbVal = 500 
# Size of the cross-validation sample for the parameters estimations
NbValeurs = 200 
# List of methods for the cross validation of the choice model
liste_methodes = c("mnlogistic")
# List of methods for the parameter estimation
liste_methodes_est = c("neuralnet")
# List of tolerances for the model choice
liste_tol_Model_Choice = c(0.05)
# List of tolerances for parameter estimations
liste_tol_estim = c(0.05)
# List of parameters to infer (alternance is removed)
liste_params_tot = c("propR","tmig","talvir","tr", "Size", "tmortMel")


#################################################

# We load the modified abc functions (originaly from Csillery et al. 2012) where we have modified a parameter of the nnet function and a parameter in gfitpca function
source(paste(directory,"/abc_function.R", sep=""))


################################################################################################
################## Model choice and parameters estimation from simulated data ##################
################################################################################################

for(l in liste_lieux){
  ############################################  Importing the simulated data
  load(paste0(directory,"/Datasets/Simulations_Wrapup_SumStats_Real_Structure_",l,".Rdata"))
  load(paste0(directory,"/Datasets/Simulations_Complete_SumStats_Real_Structure_",l,".Rdata"))
  
  # Replace the - character in the column names, otherwise it causes problems for the following
  colnames(Tableau_summary) = str_replace(colnames(Tableau_summary), '-', '_')
  colnames(Tableau_Complet) = str_replace(colnames(Tableau_Complet), '-', '_')
  
  Tableau_summary$alternance = as.character(Tableau_summary$alternance)
  Tableau_Complet$alternance = as.character(Tableau_Complet$alternance)
  
  for(stat in liste_stats){
    if(stat == "complete"){
      Tableau_simulations = get("Tableau_Complet")
    }
    if(stat == "wrapup"){
      Tableau_simulations = get("Tableau_summary")
    }
    
    ############################# Importing the real data
    TimeSerie = get(load(paste0(directory,"/Datasets/Real_TimeSerie_",l,".Rdata")))
    
    # List of real generations that are kept
    liste_generations = unique(TimeSerie$gen)
    # list of generations by compartment
    liste_generations_S = sort(unique(TimeSerie[TimeSerie$nbSd>=5,]$gen))
    liste_generations_R = sort(unique(TimeSerie[TimeSerie$nbRd>=5,]$gen))
    liste_generations_A = sort(unique(TimeSerie[TimeSerie$nbAd>=5,]$gen))
    
    ######### Adaptation of real data for two indices: G/N and rbarD
    # For rbarD and G/N, they must be compared with an equal number of loci, so the method used in python (removal of loci with NA by pop) is not satisfactory
    # We replace these two indices by the values calculated with poppR (Kamvar et al. 2014)
    
    # Importing the real data with calculations from poppr
    load(paste(directory,"/Datasets/Table_PPR_restreint_GdP_",l,".Rdata",sep=""))
    
    ############### If needed, create the wrapup table for the real data
    if(stat == "wrapup"){
      Tableau_reel = data.frame(matrix(ncol=1,nrow=1))
      
      for(hote in c("S","R","A")){
        liste_indice = get(paste0("Liste_indices_",hote))
        liste_gen = get(paste0("liste_generations_",hote))
        
        for (indice in liste_indice){
          y = TimeSerie[TimeSerie$gen %in% liste_gen,indice]
          
          ######### WRAPUP DATASET #########
          # If there are only NAs for the index under consideration (e.g. extinction or Avr fixation):
          if (sum(!is.na(y))==0){
            Tableau_reel[1,paste0(indice,"_median")] = NA
            Tableau_reel[1,paste0(indice,"_min")] = NA
            Tableau_reel[1,paste0(indice,"_max")] = NA
            Tableau_reel[1,paste0(indice,"_aire")] = NA
            Tableau_reel[1,paste0(indice,"_variance")] = NA
            Tableau_reel[1,paste0(indice,"_mean")] = NA
          }else{
            Tableau_reel[1,paste0(indice,"_median")] = median(y,na.rm=TRUE)
            Tableau_reel[1,paste0(indice,"_min")] = min(y,na.rm=TRUE)
            Tableau_reel[1,paste0(indice,"_max")] = max(y,na.rm=TRUE)
            Tableau_reel[1,paste0(indice,"_aire")] = sum(y,na.rm=TRUE)
            Tableau_reel[1,paste0(indice,"_variance")] = var(y,na.rm=TRUE)
            Tableau_reel[1,paste0(indice,"_mean")] = mean(y,na.rm=TRUE)
          }
        }
      }
      
      # Creating index lists with wrapup particles, for the summary statistics used in the analyses only: min, max, mean and variance
      Liste_indices_stats_S = c(paste0(Liste_indices_S,"_min"),paste0(Liste_indices_S,"_max"),paste0(Liste_indices_S,"_mean"),paste0(Liste_indices_S,"_variance"))
      Liste_indices_stats_R = c(paste0(Liste_indices_R,"_min"),paste0(Liste_indices_R,"_max"),paste0(Liste_indices_R,"_mean"),paste0(Liste_indices_R,"_variance"))
      Liste_indices_stats_A = c(paste0(Liste_indices_A,"_min"),paste0(Liste_indices_A,"_max"),paste0(Liste_indices_A,"_mean"),paste0(Liste_indices_A,"_variance"))
    }
    ############### If needed, create the Complete table for the real data
    if(stat == "complete"){
      Tableau_reel = data.frame(matrix(ncol=1,nrow=1))
      
      for(hote in c("S","R","A")){
        liste_indice = get(paste0("Liste_indices_",hote))
        liste_gen = get(paste0("liste_generations_",hote))
        
        for (indice in liste_indice){
          Gen_et_indice = TimeSerie[TimeSerie$gen %in% liste_gen,c("gen",indice)]
          
          ######### COMPLETE DATASET #########
          Tableau_reel[1,paste0(indice,"_",liste_gen)] = Gen_et_indice[,indice]
        }
      }
      colnames(Tableau_reel) = str_replace(colnames(Tableau_reel), '-', '_')
      
      # Creating index lists with particles adapted to the Complete table: index_generation
      Liste_indices_stats_S = c()
      Liste_indices_stats_R = c()
      Liste_indices_stats_A = c()
      for(i in Liste_indices_S){
        Liste_indices_stats_S = c(Liste_indices_stats_S,paste0(i,"_", liste_generations_S))
      }
      for(i in Liste_indices_R){
        Liste_indices_stats_R = c(Liste_indices_stats_R,paste0(i,"_", liste_generations_R))
      }
      for(i in Liste_indices_A){
        Liste_indices_stats_A = c(Liste_indices_stats_A,paste0(i,"_", liste_generations_A))
      }
    }
    
    # Sometimes values of freqavr = NA for the real data (Complete table) or variance = NA (wrapup table): we remove them from the list of indices
    if(sum(is.na(Tableau_reel[1,Liste_indices_stats_S]))>0){
      Liste_indices_stats_S = Liste_indices_stats_S[-which(is.na(Tableau_reel[1,Liste_indices_stats_S]))]
    }
    if(sum(is.na(Tableau_reel[1,Liste_indices_stats_R]))>0){
      Liste_indices_stats_R = Liste_indices_stats_R[-which(is.na(Tableau_reel[1,Liste_indices_stats_R]))]
    }
    if(sum(is.na(Tableau_reel[1,Liste_indices_stats_A]))>0){
      Liste_indices_stats_A = Liste_indices_stats_A[-which(is.na(Tableau_reel[1,Liste_indices_stats_A]))]
    }
    
    # Remove all rows that contain at least one NA in the indices
    Tableau_simulations_sans_NA_S =  Tableau_simulations[rowSums(is.na(Tableau_simulations[,Liste_indices_stats_S]))==0,]
    Tableau_simulations_sans_NA_S_R =  Tableau_simulations[rowSums(is.na(Tableau_simulations[,c(Liste_indices_stats_S,Liste_indices_stats_R)]))==0,]
    Tableau_simulations_sans_NA_S_A =  Tableau_simulations[rowSums(is.na(Tableau_simulations[,c(Liste_indices_stats_S,Liste_indices_stats_A)]))==0,]
    Tableau_simulations_sans_NA_S_R_A =  Tableau_simulations[rowSums(is.na(Tableau_simulations[,c(Liste_indices_stats_S,Liste_indices_stats_R,Liste_indices_stats_A)]))==0,]
    
    ##########################################################################
    ############################################ Cross Validation Model Choice
    ##########################################################################
    
    ################ Compartment S
    
    for(Methode in liste_methodes){
      for (Tol in liste_tol_Model_Choice){
        cv.modsel <- cv4postpr(index = as.vector(as.factor(Tableau_simulations_sans_NA_S$alternance)), sumstat = Tableau_simulations_sans_NA_S[,Liste_indices_stats_S], nval=NbVal, tol=Tol, method=Methode)
        s <- summary(cv.modsel)
        # Extract only the probability table
        write.csv(data.frame(s[2]),paste0(directory,"/Results/REEL_VALIDATION_Confusion_Matrix_ModelChoice_S_TOL",Tol,"_METHOD",Methode,"_NVAL_",NbVal,"_",stat,"_",l,".csv"), row.names = TRUE)
      }
    }
    
    ################ Compartments S and R
    for(Methode in liste_methodes){
      for (Tol in liste_tol_Model_Choice){
        
        cv.modsel <- cv4postpr(index = as.vector(as.factor(Tableau_simulations_sans_NA_S_R$alternance)), sumstat = Tableau_simulations_sans_NA_S_R[,c(Liste_indices_stats_S,Liste_indices_stats_R)], nval=NbVal, tol=Tol, method=Methode)
        s <- summary(cv.modsel)
        # Extract only the probability table
        write.csv(data.frame(s[2]),paste0(directory,"/Results/REEL_VALIDATION_Confusion_Matrix_ModelChoice_S_R_TOL",Tol,"_METHOD",Methode,"_NVAL_",NbVal,"_",stat,"_",l,".csv"), row.names = TRUE)
      }
    }
    
    ##########################################################################
    ################################### Cross Validation Parameter estimations
    ##########################################################################
    
    for (tol in liste_tol_estim){
      
      ################ Compartments S and A
      for(meth in liste_methodes_est){
        cv.res.rej <- cv4abc_custom(param = data.frame(Tableau_simulations_sans_NA_S_A[Tableau_simulations_sans_NA_S_A$alternance=="True",liste_params_tot]), sumstat= data.frame(Tableau_simulations_sans_NA_S_A[Tableau_simulations_sans_NA_S_A$alternance=="True", c(Liste_indices_stats_S,Liste_indices_stats_A)]), nval=NbValeurs, tols=tol, method=meth)
        Table_res = data.frame(cv.res.rej$true)
        for (i in c(1:length(liste_params_tot))){
          Table_res[,paste0(liste_params_tot[i],"_est")] = as.factor(data.frame(cv.res.rej$estim[[1]])[,i])
        }
        
        # Save the result
        write.csv(data.frame(Table_res),paste0(directory,"/Results/REEL_VALIDATION_Resultats_Parameters_Estimations_AvecAlt_S_A_",meth,"_TOL",Tol,"_nval", dim(Table_res)[1],"_",stat,"_",l,".csv"), row.names = TRUE)
      }
      
      ################ Compartments S R and A
      for(meth in liste_methodes_est){
        cv.res.rej <- cv4abc_custom(param = data.frame(Tableau_simulations_sans_NA_S_R_A[Tableau_simulations_sans_NA_S_R_A$alternance=="True",liste_params_tot]), sumstat= data.frame(Tableau_simulations_sans_NA_S_R_A[Tableau_simulations_sans_NA_S_R_A$alternance=="True", c(Liste_indices_stats_S,Liste_indices_stats_R,Liste_indices_stats_A)]), nval=NbValeurs, tols=tol, method=meth)
        Table_res = data.frame(cv.res.rej$true)
        for (i in c(1:length(liste_params_tot))){
          Table_res[,paste0(liste_params_tot[i],"_est")] = as.factor(data.frame(cv.res.rej$estim[[1]])[,i])
        }
        
        # Save the result
        write.csv(data.frame(Table_res),paste0(directory,"/Results/REEL_VALIDATION_Resultats_Parameters_Estimations_AvecAlt_S_R_A_",meth,"_TOL",Tol,"_nval", dim(Table_res)[1],"_",stat,"_",l,".csv"), row.names = TRUE)
      }
    }
  }
}
