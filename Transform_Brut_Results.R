# If the initial dataset is the .txt dataset directly coming from the .exe code, perform some necessary transformations
# To obtain the Rdata dataset used for further analyses
# Do not use this script if you want to use the simulation results used in the article, the results file Simulation_dataset.Rdata is already provided

# Name of the dataset file, to adapt
name_dataset = "Brut_Simulation_Results.txt"

directory = getwd()

# Import of the brut simulation table
Tableau3 = read.table(paste(directory, "/Datasets/",name_dataset, sep = ""), sep="\t", comment.char = "%", header = FALSE, fill=TRUE)

# Adaptation of the simulation table

# Remove the last empty column
Tableau3$V112 = NULL

# Name columns
colnames(Tableau3) = Tableau3[1,]

# Remove the first row (initially the column names)
Tableau3= Tableau3[-c(1),]

# Replace # and @ by NA
Tableau3[Tableau3 == "#"] <- NA
Tableau3[Tableau3 == "@"] <- NA

# Put all the columns in numeric (column by column to avoid memory problems, except for the column 'alternance', wich is a factor)
for (i in colnames(Tableau3)){
  if (i != "alternance"){
    Tableau3[,i]= as.numeric(Tableau3[,i])
  }
}
Tableau3$alternance = as.factor(Tableau3$alternance)

Tableau3=as.data.frame(Tableau3)

# Delete the 3 columns named COLDIESE
Tableau3$COLDIESE1 = NULL
Tableau3$COLDIESE2 = NULL
Tableau3$COLDIESE3 = NULL

# Add column propR :
Tableau3$propR = Tableau3$nbcapchaR/(Tableau3$nbcapchaR+Tableau3$nbcapchaS)

# For freq_avr_S freq_avr_A and freq_avr_R, if there is no individual in the compartment concerned, set the frequency to NA instead of 0 :
Tableau3[Tableau3$nbSd==0 & !is.na(Tableau3$freq_avr_S),]$freq_avr_S = NA
Tableau3[Tableau3$nbAd==0 & !is.na(Tableau3$freq_avr_A),]$freq_avr_A = NA
Tableau3[Tableau3$nbRd==0 & !is.na(Tableau3$freq_avr_R),]$freq_avr_R = NA

# Create another table, Tableau_fix, where all the simulations that do not contain virulent avr alleles at the last generation (because of population extinction or Avr fixation before the end) are removed

# List of simulations numbers that we want to keep to remove extinctions
Liste_Nsimu_sansExt = na.omit(Tableau3[Tableau3$gen==395,]$Nsimu)
# List of simulations numbers that we want to keep to remove simulation with Avr fixation
# We evaluate the frequency of avr alleles (that must be>0) at the last simulated generation on S (393)
Liste_Nsimu_sansAvr = na.omit(Tableau3[Tableau3$gen==393 & Tableau3$freq_avr_S>0 & !is.na(Tableau3$freq_avr_S),]$Nsimu)

# We keep only the simulation numbers that are in the two previous lists
Liste_Nsimu = intersect(Liste_Nsimu_sansExt,Liste_Nsimu_sansAvr)

# Remove the few simus with na in gen (there correspond to simulations where the extinction took place after the last sampled generation (395))
Tableau_fix = Tableau3[Tableau3$Nsimu %in% Liste_Nsimu & !is.na(Tableau3$gen) & !is.na(Tableau3$Nsimu),]

liste_generations = unique(Tableau_fix$gen)

# Add the column Size
Tableau_fix$Size = Tableau_fix$nbcapchaR+Tableau_fix$nbcapchaS

# Remove the spaces before and after the variable names
colnames(Tableau_fix)= trimws(colnames(Tableau_fix))

# Replace * by .
Tableau_fix$'r_value.r_value_S' = Tableau_fix$'r_value*r_value_S'
Tableau_fix$'r_value.r_value_A' = Tableau_fix$'r_value*r_value_A'
Tableau_fix$'r_value.r_value_R' = Tableau_fix$'r_value*r_value_R'

# Add 2 columns: the frequency of virulent individuals on S and A (virulent alleles squared)
Tableau_fix$freq_virulent_S = Tableau_fix$freq_avr_S * Tableau_fix$freq_avr_S
Tableau_fix$freq_virulent_A = Tableau_fix$freq_avr_A * Tableau_fix$freq_avr_A

colnames(Tableau_fix) = str_replace(colnames(Tableau_fix), '-', '_')

# Export of the refined simulation table, used for further ABC analyses
save(Tableau_fix, file = paste0(directory,"/Datasets/Simulation_dataset.Rdata"))